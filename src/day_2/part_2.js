const {
    inputs,
    gravityAssisst
} = require('./part_1');

function reverseGravityAssisst(numbers) {

    for (let i=0; i<100; i++) {
        for (let j=0; j<100; j++) {
            const deepCopy = [...numbers];
            deepCopy[1] = i;
            deepCopy[2] = j;
            if (gravityAssisst(deepCopy) == 19690720) {
                found = true;
                return 100 * i + j
            }
        }    
    }

}

console.log('Result of noun and verb is: ', reverseGravityAssisst([...inputs]));