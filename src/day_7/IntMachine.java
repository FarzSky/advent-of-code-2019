package day_7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IntMachine {

    public List<Integer> software;
    public List<Integer> input;
    public int output = 0;
    public int i = 0;
    public boolean halted = false;

    public IntMachine(int inputPhase, List<Integer> software) {
        this.input = new ArrayList<>();
        this.input.add(inputPhase);
        this.software = new ArrayList<>(software);
    }

    public Integer intCode(Integer inputSignal) {

        input.add(inputSignal);

        int softwareLen = software.size();

        while (i < softwareLen) {

            // fill opcode with lead zeros to have at least 5 numbers
            String filledOpcode = "00000" + software.get(i).toString();
            int opcodeLen = filledOpcode.length();

            // get opcode and param modes
            int opcode = Integer.parseInt(String.valueOf(filledOpcode.charAt(opcodeLen-2)) + String.valueOf(filledOpcode.charAt(opcodeLen-1)));
            int paramMode1 = Integer.parseInt(String.valueOf(filledOpcode.charAt(opcodeLen-3)));
            int paramMode2 = Integer.parseInt(String.valueOf(filledOpcode.charAt(opcodeLen-4)));
            int paramMode3 = Integer.parseInt(String.valueOf(filledOpcode.charAt(opcodeLen-5))); // its always by position, but using by value actually used then as address

            int firstArg = getParam(paramMode1, software, i + 1);
            int secondArg = getParam(paramMode2, software, i + 2);
            int thirdArg = getParam(1, software, i + 3); // get by value, cuz that value is later used as address actually

            if (opcode == 1)
            {
                software.set(thirdArg, firstArg + secondArg);
                i += 4;
            } else if (opcode == 2)
            {
                software.set(thirdArg, firstArg * secondArg);
                i += 4;
            } else if (opcode == 3)
            {
                int address = software.get(i + 1);
                software.set(address, this.input.remove(0));
                i += 2;
            } else if (opcode == 4)
            {
                output = software.get(software.get(i + 1));
                i += 2;
                return output;
            } else if (opcode == 5)
            {
                if (firstArg != 0) i = secondArg;
                else i += 3;
            } else if (opcode == 6)
            {
                if (firstArg == 0) i = secondArg;
                else i += 3;
            } else if (opcode == 7)
            {
                if (firstArg < secondArg) software.set(thirdArg, 1);
                else software.set(thirdArg, 0);
                i += 4;
            } else if (opcode == 8)
            {
                if (firstArg == secondArg) software.set(thirdArg, 1);
                else software.set(thirdArg, 0);
                i += 4;
            } else if (opcode == 99)
            {
                this.halted = true;
                return output;
            } else
            {
                i += 4;
            }

        }

        return output;

    }

    public int getParam(int paramMode, List<Integer> software, int index) {
        if (index > software.size() - 1 || software.get(index) > software.size() - 1) return 0;
        if (paramMode == 1) return software.get(index);
        else return software.get(software.get(index));
    }

}