package day_7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Part_1 {

    public static List<Integer> numbers = Arrays.asList(3,8,1001,8,10,8,105,1,0,0,21,38,47,64,89,110,191,272,353,434,99999,3,9,101,4,9,9,102,3,9,9,101,5,9,9,4,9,99,3,9,1002,9,5,9,4,9,99,3,9,101,2,9,9,102,5,9,9,1001,9,5,9,4,9,99,3,9,1001,9,5,9,102,4,9,9,1001,9,5,9,1002,9,2,9,1001,9,3,9,4,9,99,3,9,102,2,9,9,101,4,9,9,1002,9,4,9,1001,9,4,9,4,9,99,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,99);

    public static void main(String[] args) {

        String phase = "01234";
        List<String> permuts = new ArrayList<>();
        permuts = permute(phase, 0, 4, permuts);

        List<Integer> results = new ArrayList<>();
        permuts.forEach(perm -> {
            int signal = 0;
            for (String p: perm.split("")) {
                signal = intCode(Integer.parseInt(p), signal, numbers);
            }
            results.add(signal);
        });

        System.out.println("Maximum signal is: " + Collections.max(results));

    }

    public static Integer intCode(int inputPhase, int inputSignal, List<Integer> software) {

        int output = 0;

        boolean isFirstInput = true;

        int softwareLen = software.size();

        int i = 0;
        while (i < softwareLen) {

            // fill opcode with lead zeros to have at least 5 numbers
            String filledOpcode = "00000" + software.get(i).toString();
            int opcodeLen = filledOpcode.length();

            // get opcode and param modes
            int opcode = Integer.parseInt(String.valueOf(filledOpcode.charAt(opcodeLen-2)) + String.valueOf(filledOpcode.charAt(opcodeLen-1)));
            int paramMode1 = Integer.parseInt(String.valueOf(filledOpcode.charAt(opcodeLen-3)));
            int paramMode2 = Integer.parseInt(String.valueOf(filledOpcode.charAt(opcodeLen-4)));
            int paramMode3 = Integer.parseInt(String.valueOf(filledOpcode.charAt(opcodeLen-5))); // its always by position, but using by value actually used then as address

            int firstArg = getParam(paramMode1, software, i + 1);
            int secondArg = getParam(paramMode2, software, i + 2);
            int thirdArg = getParam(1, software, i + 3); // get by value, cuz that value is later used as address actually

            if (opcode == 1) {
                software.set(thirdArg, firstArg + secondArg);
                i += 4;
            } else if (opcode == 2) {
                software.set(thirdArg, firstArg * secondArg);
                i += 4;
            } else if (opcode == 3) {
                int address = software.get(i + 1);
                if (isFirstInput == true) {
                    software.set(address, inputPhase);
                    isFirstInput = false;
                } else {
                    software.set(address, inputSignal);
                }
                i += 2;
            } else if (opcode == 4) {
                output = software.get(software.get(i + 1));
                i += 2;
            } else if (opcode == 5) {
                if (firstArg != 0) i = secondArg;
                else i += 3;
            } else if (opcode == 6) {
                if (firstArg == 0) i = secondArg;
                else i += 3;
            } else if (opcode == 7) {
                if (firstArg < secondArg) software.set(thirdArg, 1);
                else software.set(thirdArg, 0);
                i += 4;
            } else if (opcode == 8) {
                if (firstArg == secondArg) software.set(thirdArg, 1);
                else software.set(thirdArg, 0);
                i += 4;
            } else if (opcode == 99) {
                return output;
            } else {
                i += 4;
            }

        }

        return output;

    }

    public static String swap(String value, int i, int j) {
        String[] splited = value.split("");
        String c = splited[i];
        splited[i] = splited[j];
        splited[j] = c;
        return String.join("", splited);
    }

    public static List<String> permute(String str, int l, int r, List<String> permuts) {
        if (l == r) permuts.add(str);
        else {
            for (int i=l; i<=r; i++) {
                str = swap(str, l, i);
                permuts = permute(str, l+1, r, permuts);
                str = swap(str, l, i);
            }
        }
        return permuts;
    }

    public static int getParam(int paramMode, List<Integer> software, int index) {
        if (index > software.size() || software.get(index) > software.size()) return 0;
        if (paramMode == 1) return software.get(index);
        else return software.get(software.get(index));
    }

}
