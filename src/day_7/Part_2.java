package day_7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Part_2 {

    public static List<Integer> numbers = Arrays.asList(3,8,1001,8,10,8,105,1,0,0,21,38,47,64,89,110,191,272,353,434,99999,3,9,101,4,9,9,102,3,9,9,101,5,9,9,4,9,99,3,9,1002,9,5,9,4,9,99,3,9,101,2,9,9,102,5,9,9,1001,9,5,9,4,9,99,3,9,1001,9,5,9,102,4,9,9,1001,9,5,9,1002,9,2,9,1001,9,3,9,4,9,99,3,9,102,2,9,9,101,4,9,9,1002,9,4,9,1001,9,4,9,4,9,99,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,99);

    public static void main(String[] args) {

        String phase = "56789";
        List<String> permuts = new ArrayList<>();
        permuts = permute(phase, 0, 4, permuts);

        List<Integer> results = new ArrayList<>();
        permuts.forEach(perm -> {
            results.add(Process(perm, numbers));
        });

        System.out.println("Maximum signal is: " + Collections.max(results));

    }

    public static int Process(String permutation, List<Integer> software) {

        String[] perms = permutation.split("");

        IntMachine A = new IntMachine(Integer.parseInt(perms[0]), software);
        IntMachine B = new IntMachine(Integer.parseInt(perms[1]), software);
        IntMachine C = new IntMachine(Integer.parseInt(perms[2]), software);
        IntMachine D = new IntMachine(Integer.parseInt(perms[3]), software);
        IntMachine E = new IntMachine(Integer.parseInt(perms[4]), software);

        int inputSignal = 0;

        while (true) {
            inputSignal = A.intCode(inputSignal);
            inputSignal = B.intCode(inputSignal);
            inputSignal = C.intCode(inputSignal);
            inputSignal = D.intCode(inputSignal);
            inputSignal = E.intCode(inputSignal);

            if (E.halted) break;

        }

        return inputSignal;

    }

    public static String swap(String value, int i, int j) {
        String[] splited = value.split("");
        String c = splited[i];
        splited[i] = splited[j];
        splited[j] = c;
        return String.join("", splited);
    }

    public static List<String> permute(String str, int l, int r, List<String> permuts) {
        if (l == r) permuts.add(str);
        else {
            for (int i=l; i<=r; i++) {
                str = swap(str, l, i);
                permuts = permute(str, l+1, r, permuts);
                str = swap(str, l, i);
            }
        }
        return permuts;
    }

}
