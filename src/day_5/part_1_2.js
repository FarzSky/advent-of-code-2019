const numbers = [3,225,1,225,6,6,1100,1,238,225,104,0,1102,17,65,225,102,21,95,224,1001,224,-1869,224,4,224,1002,223,8,223,101,7,224,224,1,224,223,223,101,43,14,224,1001,224,-108,224,4,224,102,8,223,223,101,2,224,224,1,223,224,223,1101,57,94,225,1101,57,67,225,1,217,66,224,101,-141,224,224,4,224,102,8,223,223,1001,224,1,224,1,224,223,223,1102,64,34,225,1101,89,59,225,1102,58,94,225,1002,125,27,224,101,-2106,224,224,4,224,102,8,223,223,1001,224,5,224,1,224,223,223,1102,78,65,225,1001,91,63,224,101,-127,224,224,4,224,102,8,223,223,1001,224,3,224,1,223,224,223,1102,7,19,224,1001,224,-133,224,4,224,102,8,223,223,101,6,224,224,1,224,223,223,2,61,100,224,101,-5358,224,224,4,224,102,8,223,223,101,3,224,224,1,224,223,223,1101,19,55,224,101,-74,224,224,4,224,102,8,223,223,1001,224,1,224,1,224,223,223,1101,74,68,225,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,107,677,677,224,102,2,223,223,1006,224,329,1001,223,1,223,1008,226,677,224,102,2,223,223,1006,224,344,1001,223,1,223,7,226,677,224,102,2,223,223,1005,224,359,1001,223,1,223,8,226,226,224,102,2,223,223,1006,224,374,1001,223,1,223,1007,226,226,224,102,2,223,223,1006,224,389,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,404,101,1,223,223,1108,677,226,224,102,2,223,223,1006,224,419,1001,223,1,223,1108,226,677,224,102,2,223,223,1006,224,434,101,1,223,223,1108,677,677,224,1002,223,2,223,1005,224,449,101,1,223,223,1008,677,677,224,1002,223,2,223,1006,224,464,101,1,223,223,7,677,226,224,1002,223,2,223,1006,224,479,101,1,223,223,108,677,677,224,1002,223,2,223,1005,224,494,101,1,223,223,107,226,677,224,1002,223,2,223,1006,224,509,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,524,1001,223,1,223,1107,226,677,224,1002,223,2,223,1006,224,539,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,554,1001,223,1,223,8,226,677,224,1002,223,2,223,1006,224,569,101,1,223,223,1007,677,677,224,102,2,223,223,1005,224,584,1001,223,1,223,1107,677,226,224,1002,223,2,223,1006,224,599,101,1,223,223,7,226,226,224,1002,223,2,223,1005,224,614,101,1,223,223,108,677,226,224,1002,223,2,223,1005,224,629,1001,223,1,223,108,226,226,224,1002,223,2,223,1005,224,644,101,1,223,223,1007,677,226,224,1002,223,2,223,1006,224,659,101,1,223,223,1107,226,226,224,102,2,223,223,1005,224,674,1001,223,1,223,4,223,99,226];

function main() {
    
    let i = 0;

    while (i < numbers.length) {

        const opcodeFilled = "00000" + numbers[i].toString();
        const opLen = opcodeFilled.length;

        const opcode = Number(opcodeFilled[opLen-2] + opcodeFilled[opLen-1]);
        let paraMode1 = Number(opcodeFilled[opLen-3]);
        let paraMode2 = Number(opcodeFilled[opLen-4]);

        let firstNum = getValueByParamMode(numbers, paraMode1, i+1);
        let secondNum = getValueByParamMode(numbers, paraMode2, i+2);
        
        if (opcode == 1) {
            i = getForOpcode1(firstNum, secondNum, i, numbers);
        } else if (opcode == 2) {
            i = getForOpcode2(firstNum, secondNum, i, numbers);
        } else if (opcode == 99) {
            return numbers[0];
        } else if (opcode == 5) {
            i = getForOpcode5(i, firstNum, secondNum);
        } else if (opcode == 6) {
            i = getForOpcode6(i, firstNum, secondNum);
        } else if (opcode == 7) {
            i = getForOpcode7(i, firstNum, secondNum, numbers);
        } else if (opcode == 8) {
            i = getForOpcode8(i, firstNum, secondNum, numbers);
        } else if (opcode == 3) {
            i = getForOpcode3(numbers, i);
        } else if (opcode == 4) {
            i = getForOpcode4(numbers, i);
        } else {
            i += 4;
            continue;
        }
    }

}

function getValueByParamMode(inputList, paramMode, forIndex) {
    if (paramMode == 1) return inputList[forIndex];
    else return inputList[inputList[forIndex]];
}

function getForOpcode1(firstNum, secondNum, instructionIndex, inputList) {
    result = firstNum + secondNum;
    inputList[inputList[instructionIndex+3]] = result;
    return instructionIndex + 4;
}

function getForOpcode2(firstNum, secondNum, instructionIndex, inputList) {
    result = firstNum * secondNum;
    inputList[inputList[instructionIndex+3]] = result;
    return instructionIndex + 4;
}

function getForOpcode3(inputList, instructionIndex) {
    const firstAddress = inputList[instructionIndex+1];
    inputList[firstAddress] = 5;
    return instructionIndex + 2;
}

function getForOpcode4(inputList, instructionIndex) {
    const firstAddress = inputList[instructionIndex+1];
    console.log("Opcode 4 output: ", numbers[firstAddress]);
    return instructionIndex + 2;
}

function getForOpcode5(instructionIndex, firstNum, secondNum) {
    if (firstNum !== 0) {
        instructionIndex = secondNum;
    } else {
        instructionIndex += 3;
    }
    return instructionIndex;
}

function getForOpcode6(instructionIndex, firstNum, secondNum) {
    if (firstNum === 0) {
        instructionIndex = secondNum;
    } else {
        instructionIndex += 3;
    }
    return instructionIndex;
}

function getForOpcode7(instructionIndex, firstNum, secondNum, inputList) {
    if (firstNum < secondNum) {
        inputList[inputList[instructionIndex+3]] = 1;
    } else {
        inputList[inputList[instructionIndex+3]] = 0;
    }
    return instructionIndex + 4;
}

function getForOpcode8(instructionIndex, firstNum, secondNum, inputList) {
    if (firstNum === secondNum) {
        inputList[inputList[instructionIndex+3]] = 1;
    } else {
        inputList[inputList[instructionIndex+3]] = 0;
    }
    return instructionIndex + 4;
}

console.log('Result: ', main());