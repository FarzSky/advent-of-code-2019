const input = "273025-767253";

function main(inputs) {

     const first = inputs.split('-')[0];
     const second = inputs.split('-')[1];

     const combs = [];

     for (let i=first; i<=second; i++) {

        const code = i.toString();

        let haveDouble = {};
        let decreasing = false;

        for (let j=0; j<=4; j++) {
            
            let now = Number(code[j]);
            let next = Number(code[j+1]);

            if (next < now) {
                decreasing = true;
                break;
            } else if  (now === next) {
                if (haveDouble[now]) {
                    haveDouble[now] += 2
                } else {
                    haveDouble[now] = 1
                }
            }
        }

        let doubles = false;
        Object.keys(haveDouble).forEach(key => {
            if (haveDouble[key] == 1) {
                doubles = true;
            }
        });

        if (!decreasing && doubles) {
            combs.push(i);
        }

     }

     return combs;

}

console.log('Result is: ', main(input).length);