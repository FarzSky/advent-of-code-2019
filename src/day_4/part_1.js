const input = "273025-767253";

const inputDemo1 = [];
const inputDemo2 = [];
const inputDemo3 = [];

function main(inputs) {

     const first = inputs.split('-')[0];
     const second = inputs.split('-')[1];

     const combs = [];

     for (let i=first; i<=second; i++) {

        const code = i.toString();

        let haveDouble = false;
        let decreasing = false;

        for (let j=0; j<=4; j++) {
            
            let now = Number(code[j]);
            let next = Number(code[j+1]);

            if (next < now) {
                decreasing = true;
                break;
            } else if  (now === next) {
                haveDouble = true;
            }
        }

        if (!decreasing && haveDouble) {
            combs.push(i);
        }

     }

     return combs;

}

console.log('Result is: ', main(input).length);