const {
    fuelForMass,
    moduleMasses
} = require('./part_1');

function totalFuelRequirement(masses) {

    let totalFuel = 0;

    for (let mass of masses) {
        
        let fuelReq = fuelForMass(mass);
        let currentMassSum = fuelReq;

        while (fuelReq > 0) {
            fuelReq = fuelForMass(fuelReq);
            if (fuelReq < 0) fuelReq = 0;
            currentMassSum += fuelReq;
        }

        totalFuel += currentMassSum;
        currentMassSum = 0;
        
    }
    
    return totalFuel;

}

module.exports = { totalFuelRequirement };

console.log("Total Fuel Requirement: ", totalFuelRequirement(moduleMasses));